package service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringService {

    public String getWords(String input) {

        Matcher match = Pattern.compile("(^|\\s)([а-я]|[a-z]])+", Pattern.UNICODE_CHARACTER_CLASS).matcher(input);
        StringBuilder result = new StringBuilder();

        int i = 1;
        while (match.find()) {
            result.append(String.format("%d. ", i++))
                    .append(input, match.start() == 0 ? match.start() : match.start() + 1, match.end())
                    .append("\n");
        }

        return result.toString();
    }

    public String getLongestWord(String input) {

        Matcher match = Pattern.compile("[\\w]+", Pattern.UNICODE_CHARACTER_CLASS).matcher(input);
        String result = "";

        while (match.find()) {
            if (match.end() - match.start() > result.length()) {
                result = input.substring(match.start(), match.end());
            }
        }

        return result;
    }

    public String getShortestWord(String input) {

        Matcher match = Pattern.compile("[\\w]+", Pattern.UNICODE_CHARACTER_CLASS).matcher(input);
        String result = "";

        while (match.find()) {
            if (match.end() - match.start() < result.length() || result.length() == 0) {
                result = input.substring(match.start(), match.end());
            }
        }

        return result;
    }

    public String deleteLocalNumber(String input) {

        return input.replaceAll("((8|\\+7)[\\- ]?\\(3412\\)) ?", "");
    }

    public String templateFill(String templateString, String templateKey, String templateValue) {

        String[] keys = templateKey.split(" ");
        String[] values = templateValue.split("% ?");

       if (keys.length != values.length){
           return "Кол-во параметров не совпадает";
       }

       StringBuilder result = new StringBuilder(templateString);

        for (int i = 0; i < keys.length ; i++) {
           result.replace(0, result.length(), result.toString().replace("$"+keys[i], values[i]) );
        }

        return result.toString();

    }
}
