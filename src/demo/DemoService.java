package demo;

import service.DateService;
import service.NumberService;
import service.StringService;

import java.text.DecimalFormat;
import java.time.LocalDateTime;

public class DemoService {

    public void execute() {

        //Date
        System.out.println("Даты:");

        DateService dateService = new DateService();

        //1.1
        System.out.print("1.1 ");
        dateService.youAge(LocalDateTime.of(1993, 4, 6, 9, 0, 0));

        //1.2
        String str1 = "25.07.2007";
        String str2 = "25.07.2018";
        System.out.printf("1.2 Между %s и %s прошло %d дней\n",
                str1,
                str2,
                dateService.daysBetween(str1, str2)
        );

        //1.3
        str1 = "2016-08-16T23:11:26-11:00";
        System.out.printf("1.3 Конвертация \"%s\" в Ижевское время \"%s\"\n\n",
                str1,
                dateService.convertDate(str1)
        );

        //Other
        System.out.println("Прочее:");

        NumberService numberService = new NumberService();

        //2.1
        double d1 = 6.7;
        System.out.printf("2.1 Площадь круга с радиусом %s равна %s\n",
                new DecimalFormat("0.#######").format(d1),
                numberService.circleSquare(d1).toString()
        );

        //2.2
        str1 = "0.1";
        str2 = "0.15";
        String str3 = "0.25";
        System.out.printf("2.2 Даны три числа: %s, %s и %s. Третье число%sявляется суммой первых двух\n",
                str1, str2, str3,
                numberService.isThirdSumOfFirstTwo(str1, str2, str3) ? " " : " НЕ ");

        //2.3
        int i1 = 4;
        int i2 = 35;
        int i3 = -10;
        System.out.printf("2.3 Даны три числа: %d, %d и %d. Минимальное: %d Максимальное: %d\n\n",
                i1, i2, i3,
                numberService.findMin(i1, i2, i3),
                numberService.findMax(i1, i2, i3)
        );

        //RegularExpressions
        System.out.println("Строки и Регулярные выражения:");

        StringService stringService = new StringService();

        //4.1
        str1 = "Ребе, Ви случайно не\nзнаете, сколько тогда Иуда получил по нынешнему курсу";
        System.out.printf("4.1 Данная строка:\n\"%s\"\n" +
                        "Содержит следующие слова в нижнем регистре:\n%s",
                str1,
                stringService.getWords(str1)
        );

        //4.2
        str1 = "Ребе, Ви случайно не знаете, сколько тогда Иуда получил по нынешнему курсу";
        System.out.printf("\n4.2 В строке: \"%s\" \n" +
                        "Самое длинное слово: \"%s\" (%d), а самое короткое: \"%s\" (%d)\n\n",
                str1,
                stringService.getLongestWord(str1),
                stringService.getLongestWord(str1).length(),
                stringService.getShortestWord(str1),
                stringService.getShortestWord(str1).length()
        );

        //4.3
        str1 = "Дана строка, содержащая в себе, помимо прочего, номера телефонов +7 (3412) 517-647. Необходимо удалить из этой строки префиксы локальных\n" +
                "номеров, соответствующих Ижевску. Например, из 8(3412) 4997-12 нужно получить 4997-12";
        System.out.printf("4.3 Из строки: \n\"%s\"\n\n" +
                        "Удаляются коды телефоных номеров Ижевска: \n\"%s\"\n\n",
                str1,
                stringService.deleteLocalNumber(str1));

        //4.4
        str1 = "Уважаемый, $userName, извещаем вас о том, что на вашем счете $accountNumber скопилась сумма, превышающая стоимость\n" +
                "$countMonth месяцев пользования нашими услугами. Деньги продолжают поступать. Вероятно, вы неправильно настроили\n" +
                "автоплатеж. С уважением, $employeeFIO $employeePost";
        str2 = "employeeFIO employeePost userName accountNumber countMonth";
        str3 = "Иванов Василий Петрович% Тех. Поддержка% Петров Иван% 1234-3546-4562-4565% 5%";
        System.out.printf("4.4 Заполнение шаблона: \n\"%s\"\n\n" +
                        "С помощью строки ключей и строки значений (разделены знаком %%): \n\"%s\"\n\"%s\"\n\n" +
                        "Результат: \n\"%s\"\n",
                str1, str2, str3,
                stringService.templateFill(str1, str2, str3)
        );
    }
}
